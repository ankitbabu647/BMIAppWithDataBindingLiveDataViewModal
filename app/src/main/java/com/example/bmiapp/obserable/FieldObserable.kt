package com.example.bmiapp.obserable

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR


class FieldObserable:BaseObservable() {




    @get:Bindable
    var username:String=""
        set(value) {
            field=value
            notifyPropertyChanged(BR.username)
        }



    @get:Bindable
    var phone:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.phone)
    }




    @get:Bindable
    var height:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.height)
    }




    @get:Bindable
    var weight:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.weight)
    }





}